const fs = require('fs');

class PersonalityGenerator {
  constructor() {
  }

  generate() {
    this.zodiac = JSON.parse(fs.readFileSync('zodiac.json'));

    const jday = Math.floor(Math.random() * 365);
    const year = Math.floor(Math.random() * this.zodiac.Chinese.length);
    const chiSign = this.getChinese(year);
    const helSign = this.getHellenic(jday);
    const keywords = this.getKeywords(chiSign, helSign);
    // Adding days should put us entirely in 1999, which we know isn't a Leap Year,
    // making sure the probilities are even
    let date = new Date('January 1, 1998 12:00:00');

    date.setDate(date.getDate() + jday);
    this.result = {
      date: date,
      julianDay: jday,
      keywords: keywords,
      sign: helSign,
      year: chiSign,
    }
  }

  getChinese(year) {
    const cSign = this.zodiac.Chinese[year];

    cSign.element = this.zodiac.chineseElement
      .filter(e => e.name === cSign.element)[0];
    return cSign;
  }

  getHellenic(jday) {
    jday = jday % 366;

    // We need the sigh where the day is in its bounds
    let hSign = this.zodiac.Hellenic
      .filter(h => jday >= h.start && jday <= h.end)[0];
    let first = this.zodiac.Hellenic[0];
    let last = this.zodiac.Hellenic[this.zodiac.Hellenic.length - 1];

    // This code will probably never be called, but is wedged in on the chance that
    // the randomly generated day is out of bounds.
    if (hSign === null || typeof hSign === 'undefined') {
      const idx = Math.floor(Math.random() * this.zodiac.Hellenic.length);
      hSign = this.zodiac.Hellenic[idx];
      console.log(`Generated a day of ${jday}; picking random sign`);
    }

    hSign.element = this.zodiac.element
      .filter(e => e.name === hSign.element)[0];
    hSign.modality = this.zodiac.modality
      .filter(m => m.name === hSign.modality)[0];
    // If the date is close to the sign's boundary, then it's on the cusp
    // of another sign
    // ...except, the first and last sign are actually the same, so those
    // boundaries don't count
    if (hSign.start !== first.start && jday < hSign.start + 5) {
      hSign.cuspOf = this.getHellenic(jday - 15 + 365);
    } else if (hSign.end !== last.end && jday > hSign.end - 5) {
      hSign.cuspOf = this.getHellenic(jday + 15);
    }

    return hSign;
  }

  getKeywords(chinese, hellenic) {
    // Collect the keyword lists we know exist
    const keywords = [
      chinese.keywords,
      chinese.element.keywords,
      hellenic.element.keywords,
      hellenic.modality.keywords,
    ];
    const result = {};

    // Add the keyword lists that only come when the character is on the cusp
    // of two signs
    if (hellenic.hasOwnProperty('cuspOf')) {
      keywords.push(hellenic.cuspOf.element.keywords);
      keywords.push(hellenic.cuspOf.modality.keywords);
    }

    // Join the keyword lists, then count up the frequency of each word
    [].concat(...keywords).forEach(k => {
      if (result.hasOwnProperty(k)) {
        result[k] += 1;
      } else {
        result[k] = 1;
      }
    });
    return result;
  }
}

module.exports = PersonalityGenerator;

